<?php

namespace App\Models;

use App\Scopes\SellerScope;
use App\Transformers\SellerTransformer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use function PHPUnit\Framework\returnSelf;

class Seller extends User
{
    use HasFactory,SoftDeletes;
    public string $transformer = SellerTransformer::class;

    protected $table = 'users';
    public function __construct()
    {
        array_push($this->hidden,'admin');
    }
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new SellerScope());
    }
    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
