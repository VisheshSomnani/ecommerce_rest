CREATE TABLE `users` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `email` varchar(255),
  `password` varchar(255),
  `verified` boolean,
  `verification_token` varchar(255),
  `admin` bool
);

CREATE TABLE `buyers` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `user_id` int
);

CREATE TABLE `sellers` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `user_id` int
);

CREATE TABLE `products` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `description` text,
  `quantity` integer,
  `price` double,
  `status` varchar(255),
  `seller_id` int
);

CREATE TABLE `categories` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `description` text
);

CREATE TABLE `category_product` (
  `category_id` int,
  `product_id` int
);

CREATE TABLE `transactions` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `quantity` int,
  `buyer_id` int,
  `product_id` int
);

ALTER TABLE `users` ADD FOREIGN KEY (`id`) REFERENCES `buyers` (`user_id`);

ALTER TABLE `users` ADD FOREIGN KEY (`id`) REFERENCES `sellers` (`user_id`);

ALTER TABLE `sellers` ADD FOREIGN KEY (`id`) REFERENCES `products` (`seller_id`);

ALTER TABLE `categories` ADD FOREIGN KEY (`id`) REFERENCES `category_product` (`category_id`);

ALTER TABLE `products` ADD FOREIGN KEY (`id`) REFERENCES `category_product` (`product_id`);

ALTER TABLE `buyers` ADD FOREIGN KEY (`id`) REFERENCES `transactions` (`buyer_id`);

ALTER TABLE `products` ADD FOREIGN KEY (`id`) REFERENCES `transactions` (`product_id`);
